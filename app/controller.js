/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 * 
 */
;(function() {

  angular
    .module('boilerplate')
    .controller('MainController', MainController)
    .controller('ExplainController', ExplainController)
    .controller('RateController', RateController);

  MainController.$inject = ['$scope', '$http', 'LocalStorage', 'QueryService', '$rootScope'];
  MainController.$inject = ['$scope', '$http', 'LocalStorage', 'QueryService', '$rootScope', '$location'];
  RateController.$inject = ['$scope', '$http', 'LocalStorage', 'QueryService', '$rootScope', '$location'];


  function MainController($scope, $http, LocalStorage, QueryService, $rootScope) {

    var self = this;

    $('.collapsible').collapsible();

    $scope.users = [];

    $http({ method: 'GET', url: 'http://localhost:8000/api/vertinimas/users' }).then(function(users) {
        $scope.users = users.data;
    });

    $scope.setUserId = function(user_id){
      console.log(user_id);
      $rootScope.userId = user_id;
    }

  }

  function ExplainController($scope, $http, LocalStorage, QueryService, $rootScope, $location) {
    // if(typeof $rootScope.userId == 'undefined') {
    //     $location.path('/');
    // }

  }

    function RateController($scope, $http, LocalStorage, QueryService, $rootScope, $location) {
        // if(typeof $rootScope.userId == 'undefined') {
        //     $location.path('/');
        // }
        $('.collapsible').collapsible('open', 0);

        $scope.question = {
            one: 0,
            two: 0
        };

        $scope.collapsible = function(open) {
            $('.collapsible').collapsible('open', open);
        }

        $scope.submitRate = function(){
          $scope.question.userId = $rootScope.userId;
            console.dir($scope.question);
        }

    }


})();